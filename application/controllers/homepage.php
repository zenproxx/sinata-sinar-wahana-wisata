<!DOCTYPE html>

<html lang="en">

<head>
	
	<!-- Basic structure of websites & mobile first-->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Sinar Wahana Wisata</title>
    
	<!-- Here are the css files -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/custom.css" />
	<link rel="stylesheet" href="assets/css/motion-ui-min.css" />
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
	
</head>

<body>
	
	<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Homepage extends CI_Controller {
		
		function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->load->view('navbar');
        $this->load->view('layout/hero');
        $this->load->view('layout/layanan');
        $this->load->view('layout/paket-tour');
        $this->load->view('layout/choose-us');
		$this->load->view('footer');

	}
}

?>

	<!-- Dont forget the javascript -->
	<script src="assets/js/vendor/jquery.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	
	<!-- Insert this within your head tag and after foundation.css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/motion-ui/1.1.1/motion-ui.min.css" />
	
</body>

</html>