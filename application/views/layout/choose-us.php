<div id="choose" style="background: url(assets/images/borobudur2.jpg) top no-repeat fixed;">
    <div class="overlay">
    <div class="container">
        <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-2 col-sm-offset-2 col-xs-offset-2">
            <h2 class="text-center">Kenapa memilih layanan kami?</h2>
            <div class="col-md-2 col-sm-2 col-xs-2 col-md-offset-5 col-sm-offset-5 col-xs-offset-5 border"></div>
        </div>
    </div>
    
    <div class="container" style="padding-bottom: 50px;">
        <div class="row">
            <div class="col-md-3 col-sm-3 text-center" style="padding-bottom:10px; color: #eee;">
                <h1><i class="fa fa-fighter-jet fa-2x"></i></h1>
                <h3>Fast</h3>
                <p>Pelayanan cepat dan handal.</p>
            </div>
            
            <div class="col-md-3 col-sm-3 text-center" style="padding-bottom:10px; color: #eee;">
                <h1><i class="fa fa-handshake-o fa-2x"></i></h1>
                <h3>Easy &amp; Friendly</h3>
                <p>Pelayanan ramah dan mudah tentunya.</p>
            </div>
                    
            <div class="col-md-3 col-sm-3 text-center" style="padding-bottom:10px; color: #eee;">
                <h1><i class="fa fa-repeat fa-2x fa-spin"></i></h1>
                <h3>Update</h3>
                <p>Informasi yang didapat selalu up to date.</p>
            </div>
            
            <div class="col-md-3 col-sm-3 text-center" style="padding-bottom:10px; color: #eee;">
                <h1><i class="fa fa-plus fa-2x"></i></h1>
                <h3>Bonus</h3>
                <p>Dapatkan bonus yang tak terduga.</p>
            </div>
            
        </div>
    </div>
    </div>
    
    <div class="container-fluid" style="padding-top: 20px; padding-bottom: 40px; background: #3498db;">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-8 col-md-offset-2 col-sm-offset-2 col-xs-offset-2 text-center">
            <h2 class="text-center">Jadi tunggu apa lagi?</h2>
            <a class="btn btn-custom center">Call +62 813 7075 4319</a>
            </div>
        </div>
    </div>
</div>
