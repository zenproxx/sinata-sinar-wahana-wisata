<!-- Hero section -->
<div id="hero" style="background: url(assets/images/wings.jpeg)no-repeat center; -webkit-background-size: cover;">
    <div class="herooverlay">
        <div class="container">
            <div class="row">
            <div class="col-md-6 col-sm-6 col-sm-offset-3 col-md-offset-3 col-xs-8 col-xs-offset-2 text-center" style="margin-top: 200px; border: 7px solid #fff; padding: 10px;">
                <h2 style="text-transform: uppercase; color: white; font-size: 36px; font-weight: 700;">Sinar Wahana Wisata</h2>
                <p style="text-transform: uppercase; color: white; font-size: 20px; font-weight: 200;">Tour & travel</p>
                <p style="letter-spacing: 1px; color: #fff;"><i>Sobat Wisataku</i></p>
            </div>
            </div>
        </div>
    </div>
</div><!-- end of hero section -->
