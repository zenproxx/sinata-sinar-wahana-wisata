<div id="paket-tour" style="background:url(assets/images/tour.jpg) top center no-repeat fixed; background-size: cover;">
    <div class="overlay">
        <div class="container">

            <div class="row" style="padding-top: 50px; padding-bottom: 50px;">

                <div class="col-md-12 col-sm-12">
                    <h2 class="text-center" style="color: #fff">PAKET TOUR</h2>
                    <hr style="width: 100px; height: 5px; border: none; background: #e67e22;">
                </div>
            


                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="panel panel-default" style="background: rgba(225,225,225,.6);">
                        <div class="panel-body">
                            <h3 class="text-center">Malaysia</h3>
                            <img src="assets/images/malaysia.jpg" class="thumbnail img-responsive" alt="Malaysia">
                            <button href="" class="btn btn-primary center-block"><i class="fa fa-download"></i> Rincian</button>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="panel panel-default" style="background: rgba(225,225,225,.6);">
                        <div class="panel-body">
                            <h3 class="text-center">Singapura</h3>
                            <img src="assets/images/korea.jpg" class="thumbnail img-responsive" alt="Malaysia" />
                            <button href="" class="btn btn-primary center-block"><i class="fa fa-download"></i> Rincian</button>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="panel panel-default" style="background: rgba(225,225,225,.6);">
                        <div class="panel-body">
                            <h3 class="text-center">Japan</h3>
                            <img src="assets/images/japan.jpg" class="thumbnail img-responsive" alt="Malaysia" />
                            <button href="" class="btn btn-primary center-block"><i class="fa fa-download"></i> Rincian</button>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="panel panel-default" style="background: rgba(225,225,225,.6);">
                        <div class="panel-body">
                            <h3 class="text-center">Australia</h3>
                            <img src="assets/images/korea.jpg" class="thumbnail img-responsive" alt="Malaysia" />
                            <button href="" class="btn btn-primary center-block"><i class="fa fa-download"></i> Rincian</button>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="panel panel-default" style="background: rgba(225,225,225,.6);">
                        <div class="panel-body">
                            <h3 class="text-center">Thailand</h3>
                            <img src="assets/images/korea.jpg" class="thumbnail img-responsive" alt="Malaysia" />
                            <button href="" class="btn btn-primary center-block"><i class="fa fa-download"></i> Rincian</button>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="panel panel-default" style="background: rgba(225,225,225,.6);">
                        <div class="panel-body">
                            <h3 class="text-center">Brunei</h3>
                            <img src="assets/images/brunei.jpg" class="thumbnail img-responsive" alt="Malaysia" />
                            <button href="" class="btn btn-primary center-block"><i class="fa fa-download"></i> Rincian</button>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="panel panel-default" style="background: rgba(225,225,225,.6);">
                        <div class="panel-body">
                            <h3 class="text-center">Vietnam</h3>
                            <img src="assets/images/malaysia.jpg" class="thumbnail img-responsive" alt="Malaysia" />
                            <button href="" class="btn btn-primary center-block"><i class="fa fa-download"></i> Rincian</button>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="panel panel-default" style="background: rgba(225,225,225,.6);">
                        <div class="panel-body">
                            <h3 class="text-center">Korea</h3>
                            <img src="assets/images/korea.jpg" class="thumbnail img-responsive" alt="Malaysia" />
                            <button href="" class="btn btn-primary center-block"><i class="fa fa-download"></i> Rincian</button>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>