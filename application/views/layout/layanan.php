<!-- pelayanan -->
<div id="layanan" style="padding-top: 30px; padding-bottom: 50px;">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2" style="margin-top:10px; margin-bottom:30px;">
                <h2 class="text-center" style="text-transform: uppercase; font-weight:300;">Discover our services</h2>
                <hr style="width: 100px; height: 5px; border: none; background: #e67e22;">
            </div>
            <div class="col-md-4 col-sm-4 text-center">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1><i class="fa fa-edit fa-2x"></i></h1>
                        <h3>Booking Ticket</h3>
                        <p>Pesan tiket sekarang juga mulai dari pemesanan tiket pesawat, kereta api, dan masih banyak lagi.</p>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4 col-sm-4 text-center">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1><i class="fa fa-globe fa-2x fa-spin"></i></h1>
                        <h3>Tour & Travel</h3>
                        <p>Dapatkan penawaran touring ke luar maupun ke dalam negeri dengan pelayanan yang ramah dan harga terbaik.</p>
                    </div>
                </div>
            </div>
            
            <div class="col-md-4 col-sm-4 text-center">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1><i class="fa fa-dollar fa-2x"></i></h1>
                        <h3>Payment</h3>
                        <p>Segala jenis pembayaran mulai dari token listrik, pulsa, pembayaran tagihan internet, dan masih banyak lagi.</p>
                    </div>
                </div>
            </div>
        </div>    
    </div>

</div>