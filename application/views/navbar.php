<nav id="menu" class="navbar navbar-default" style="margin-bottom: 0px;">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand page-scroll" href="#menu"><img src="assets/images/logoo.png" width="70px" height="50px;" /></a></div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="" class="page-scroll">Blog</a></li>
        <li><a href="#layanan" class="page-scroll">Layanan</a></li>
        <li><a href="#paket-tour" class="page-scroll">Paket Tour</a></li>
        <li><a href="#about" class="page-scroll">About</a></li>
        <li><a href="#contact" class="page-scroll">Contact</a></li>
		
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
</nav>