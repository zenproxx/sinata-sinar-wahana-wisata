<!-- Footer goes here -->
<div class="container-fluid">
    <div class="row">
	
        <div class="panel panel-default" style="background-color: #3498db; color: #fff; border: none; border-radius: 0px;">
            <div class="panel-body">
			
                <div class="col-md-6 col-lg-6">
                    <div class="copyright text-center">
                        © Copyright 2017 : <a href="http://www.sinarwahanawisata.com" style="color: #e67e22;">Sinar Wahana Wisata</a>. All right reserved.
                    </div>
                </div>
				
                <div class="col-md-6 col-lg-6">
                    <div class="design text-center">
                        Made with Pride : <a href="http://www.wearekhz.com" style="color: #2c3e50;">KHz Technology</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End of footer -->

