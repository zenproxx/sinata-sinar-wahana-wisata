<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/custom.css">

<div class="container">
    <div class="row">
        <h1 class="text-center" style="color: #3498db; margin-bottom: 50px;"><b>ERROR 404</b></h1>
        <div class="panel panel-default" style="border: 4px dashed #3498db;">
            <div class="panel-body">
                <div class="col-md-12">
                    <h1 class="text-center"><b>Whoops..!</b></h1>
                    <h3 class="text-center">Something wrong here..</h3>
                    <p class="text-center">Please click the back button on your browser or click button bellow.</p>
                </div>
                <div class="col-md-4 col-md-push-4">
                    <form action="#">
                        <input type="submit" value="Homepage" href="#" class="btn btn-primary btn-block">
                    </form>
                </div>
            </div>
                    
                </div>
            </div>        
        </div>
        
        <div class="col-md-2">
